#Description of this Program
#Author : Phithak Buathong
#Program Name : Read & Write File
def r(str):#function for Read File
    with open("file.txt",'r') as f:
        data = f.read()
        print(data)
        f.close()
def accept(str):#function for Accept File
    pass
def replace(str):#function for Replace Data in File
    t = int(input("How Many to Write ? "))
    d = []
    for i in range(t):
        data = input("Please Insert Data : ")
        d.append(data)
    name = input("Please Insert File Name : ")
    with open(name+".txt",'w') as f:
        for i in d:
            f.write(i + "\n")
        f.close()
def delete(str):#function for Delete Data in File
    with open("file.txt",'w') as f:
        f.write("")
        f.close()
def add(str):#function for Add Data to File
    t = int(input("How Many to Add ? "))
    d = []
    for i in range(t):
        data = input("Please Insert Data : ")
        d.append(data)
    with open("file.txt",'a') as f:
        for i in d:
            f.write(i + "\n")
        f.close()
r(str)
select = str(input("Accept Data Type  : acc \nReplace Data Type : rep \nDelete Data Type  : del \nAdd Data Type : add\nType Here : "))
if select == 'acc':
    accept(str)
elif select == 'rep':
    replace(str)
elif select == 'del':
    delete(str)
elif select == 'add':
    add(str)

